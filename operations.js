const assert = require('assert');

exports.insertDocument = (db, document, collection, callback) => {
	const coll = db.collection(collection);
	// Promise
	return coll.insert(document);
}

exports.findDocuments = (db, collection, callback) => {
	const coll = db.collection(collection);
	// Promise
	return coll.find({}).toArray();
}

exports.removeDocument = (db, document, collection, callback) => {
	const coll = db.collection(collection);
	// Promise
	return coll.deleteOne(document);
}

exports.updateDocument = (db, document, update, collection, callback) => {
	const coll = db.collection(collection);
	// Promise
	return coll.updateOne(document, { $set: update }, null);
}